from langgraph.graph import END, StateGraph, START
import incident_rag

def create():
  workflow = StateGraph(incident_rag.GraphState)

  # Define the nodes
  workflow.add_node("retrieve", incident_rag.retrieve)  # retrieve
  workflow.add_node("grade_documents", incident_rag.grade_documents)  # grade documents
  workflow.add_node("generate", incident_rag.generate)  # generatae
  workflow.add_node("transform_query", incident_rag.transform_query)  # transform_query

  # Build graph
  workflow.add_edge(START, "retrieve")
  workflow.add_edge("retrieve", "grade_documents")
  workflow.add_conditional_edges(
      "grade_documents",
      incident_rag.decide_to_generate,
      {
          "transform_query": "transform_query",
          "generate": "generate",
      },
  )
  workflow.add_edge("transform_query", "retrieve")
  workflow.add_conditional_edges(
      "generate",
      incident_rag.grade_generation_v_documents_and_question,
      {
          "not supported": "generate",
          "useful": END,
          "not useful": "transform_query",
      },
  )

  # Compile
  app = workflow.compile()
  return app
