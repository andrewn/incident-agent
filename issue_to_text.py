from markdown_it import MarkdownIt
from mdit_plain.renderer import RendererPlain

def convert(issue, notes):
  parser = MarkdownIt(renderer_cls=RendererPlain)

  issue_text_lines = [parser.render(issue.description)]

  no_bot_notes = filter(lambda n: n.author["username"] != "gitlab-bot" and not n.system, notes)

  for note in no_bot_notes:
    issue_text_lines + [parser.render(note.body)]

  return "\n".join(issue_text_lines)
