#!/usr/bin/env bash

pip install -U langchain-nomic \
  langchain_community \
  tiktoken \
  langchainhub \
  langchain \
  langgraph \
  httpx \
  'nomic[local]' \
  marqo \
  markdown-it-py mdit_plain \
  python-gitlab

