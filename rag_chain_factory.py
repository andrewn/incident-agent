from langchain import hub
from langchain_core.output_parsers import StrOutputParser
from langchain_community.chat_models import ChatOllama

def create():
  # Prompt
  prompt = hub.pull("rlm/rag-prompt")

  # LLM
  llm = ChatOllama(model="mistral", temperature=0)

  # Chain
  rag_chain = prompt | llm | StrOutputParser()

  return rag_chain
