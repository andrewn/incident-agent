from pprint import pprint
import app_factory
import issue_to_text
import gitlab
import os

gl = gitlab.Gitlab(private_token=os.environ['GITLAB_TOKEN'])
# gl.enable_debug()

project = gl.projects.get(7444821, lazy=True)
issue = project.issues.get(18229)
notes = issue.notes.list(get_all=True)

issue_text = issue_to_text.convert(issue, notes)

# Run
inputs = {"question": "What would you do to mitigate the following production incident? Include links for related issues which may have been due to the same root cause: " + issue_text}
print(inputs)

app = app_factory.create()
for output in app.stream(inputs):
    for key, value in output.items():
        # Node
        pprint(f"Node '{key}':")
        # Optional: print full state at each node
        # pprint.pprint(value["keys"], indent=2, width=80, depth=None)
    pprint("\n---\n")

# Final generation
print(value["generation"])
