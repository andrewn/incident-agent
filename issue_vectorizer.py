import gitlab
from markdown_it import MarkdownIt
from mdit_plain.renderer import RendererPlain
import marqo
import os
import issue_to_text

# Create a marco client
mq = marqo.Client()
index_name = "incident-agent"

# Optional: clear out index before usage
# try:
#     mq.index(index_name).delete()
# except:
#     pass

# Create the index with custom settings
index_settings = {
    "model": "flax-sentence-embeddings/all_datasets_v4_MiniLM-L6",
    "normalizeEmbeddings": True,
    "textPreprocessing": {
        "splitLength": 3,
        "splitOverlap": 1,
        "splitMethod": "sentence"
    },
}
mq.create_index(index_name, settings_dict=index_settings)

# Talk to GitLab

gl = gitlab.Gitlab(private_token=os.environ['GITLAB_TOKEN'])
# gl.enable_debug()

def vectorize_issues(project_id, label):
  project = gl.projects.get(project_id, lazy=True)
  issues = project.issues.list(page=1, labels=label, per_page=100)

  for issue in issues:
    print(issue.iid)
    notes = issue.notes.list(get_all=True)

    mq.index(index_name).add_documents([{
        "issue_text": issue_to_text.convert(issue, notes),
        "issue_title": issue.title,
        "_id": issue.web_url
      }], tensor_fields=["issue_text", "title"])


vectorize_issues(7444821, "incident")
vectorize_issues(7444821, "incident-review")
