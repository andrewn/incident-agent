# Incident Agent POC

Taken from the langchain example at <https://langchain-ai.github.io/langgraph/tutorials/rag/langgraph_self_rag_local/>.

```console
$ # Install ollama: https://ollama.com/ then pull the model and serve it
$ ollama pull mistral
$ # Obtain a token for working with GitLab
$ eval $(pmv env 'Dedicated/Sandbox/GitLab.com/PAT:Personal')
$ # Run marqo for vectordb..
$ docker run -d --name marqo -p 8882:8882 marqoai/marqo:latest
$ # Vectorize some production incidents for GitLab
$ python issue_vectorizer.py
$ # Ask the RAG a question about an ongoing incident
$ python find_similar_incidents.py
$ ....
```
